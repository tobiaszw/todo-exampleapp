<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_should_be_able_to_login_as_registered_user()
    {
        $userEmail = 'testing@example.com';
        $userPassword = 'test';

        $this->createUser($userEmail, $userPassword);

        $this->visit('/')
            ->see('Login')
            ->type($userEmail, 'email')
            ->type($userPassword, 'password')
            ->press('Login')
            ->seePageIs('/projects');
    }

    /**
     * @test
     */
    public function it_should_validate_user_credentials()
    {
        $this->visit('/')
            ->see('Login')
            ->type('thisuserdosntexists@example.com', 'email')
            ->type('qwe', 'password')
            ->press('Login')
            ->seePageIs('/');
    
        $this->see('These credentials do not match our records.');
    }

    private function createUser($email, $password)
    {
        return App\User::create([
                'email' => $email,
                'password' => $password
        ]);
    }
}
