<?php
use App\Project;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProjectTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_generates_slug_from_project_name()
    {
        $project = new Project;

        $project->name = 'Example project name';

        $this->assertTrue($project->save());
        $this->assertSame('example-project-name', $project->slug);
    }

    /**
     * @test
     */
    public function it_has_immutable_slug()
    {
        $project = new Project;

        $project->name = 'Example project name';

        $this->assertTrue($project->save());

        $project->name = 'Change name during testing';

        $this->assertTrue($project->save());

        $this->assertSame('example-project-name', $project->slug);
        $this->assertSame('Change name during testing', $project->name);
    }

    /**
     * @test
     */
    public function it_add_new_task_to_project()
    {
        $task = factory(App\Task::class)->make();
        $project = factory(App\Project::class)->create();

        $project->addTask($task);

        $this->assertEquals($project->tasks->get(0)->name, $task->name);
    }
}
