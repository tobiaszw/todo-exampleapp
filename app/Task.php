<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'completed' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'completed'];

    /**
     * sluggable columns
     * 
     * @var array 
     */
    public $sluggable = ['name'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function($model) {

            if (is_null($model->slug) && isset($model->sluggable)) {

                $sluggable = array_map(function($field) use ($model) {
                    return $model->$field;
                }, $model->sluggable);

                $model->attributes['slug'] = str_slug(join('-', $sluggable));
            }
        });
    }

    /**
     * Get the project that owns the task.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Toggle task state
     * 
     * @return type
     */
    public function toggle()
    {
        $this->completed = !$this->completed;
        return $this->save();
    }
}
