<?php
Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

Route::get('/', [
    'as' => 'login',
    'uses' => 'Auth\AuthController@getLogin'
]);

Route::post('/', [
    'uses' => 'Auth\AuthController@postLogin'
]);

Route::group(['middleware' => 'auth'], function() {
    
    Route::model('projects', 'Project');

    Route::bind('projects', function($value) {
        return App\Project::whereSlug($value)->first();
    });

    Route::resource('projects', 'ProjectsController');
    Route::resource('tasks', 'TasksController');
});
