<?php

namespace App\Http;

class Flash
{

    /**
     * 
     * @param type $title
     * @param type $message
     * @param type $level
     */
    public function create($title, $message, $level = 'info')
    {
        session()->flash('flash_message', [
            'title' => $title,
            'message' => $message,
            'level' => $level
        ]);
    }

    /**
     * 
     * @param type $title
     * @param type $message
     * @return type
     */
    public function success($title, $message)
    {
        return $this->create($title, $message, 'success');
    }

    /**
     * 
     * @param type $title
     * @param type $message
     * @return type
     */
    public function error($title, $message)
    {
        return $this->create($title, $message, 'error');
    }

    /**
     * 
     * @param type $title
     * @param type $message
     * @return type
     */
    public function warning($title, $message)
    {
        return $this->create($title, $message, 'warning');
    }
}
