<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Description of Context
 *
 * @author Tobiasz Wawrzynczok <tobiasz@codable.pl>
 */
class Context extends Facade
{

    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'context';
    }
}
