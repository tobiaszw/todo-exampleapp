<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    /**
     * sluggable columns
     * 
     * @var array 
     */
    public $sluggable = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function($model) {

            if (is_null($model->slug) && isset($model->sluggable)) {

                $sluggable = array_map(function($field) use ($model) {
                    return $model->$field;
                }, $model->sluggable);

                $model->attributes['slug'] = str_slug(join('-', $sluggable));
            }
        });
    }

    /**
     * Get the tasks for the project.
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * add taks to project
     * 
     * @param \App\Task $task
     * @return type
     */
    public function addTask(Task $task)
    {
        return $this->tasks()->save($task);
    }
}
