<?php
use Illuminate\Database\Seeder;

class ProjectsAndTasksSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Project::class, 3)
            ->create()
            ->each(function($project) {
                $project->tasks()->saveMany(factory(App\Task::class, 10)->make());
            });
    }
}
