<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel example app</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        @yield('style')
        {!! Html::style('css/app.css') !!}
    </head>
    <body> 

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Example App L5</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">{{trans('projects')}} <span class="sr-only">(current)</span></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if(Auth::check())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->email}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#edit-profile-todo">{{trans('edit')}}</a></li>
                                <li><a href="{{route('logout')}}">{{trans('logout')}}</a></li>
                            </ul>
                        </li>
                        @else
                        <li><a href="{{route('login')}}">Login</a></li>
                        @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">
            @if($errors->has())
            <div class="alert alert-danger">
                <button class="close">&times;</button>
                @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            @yield('body')
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
        {!!Html::script('js/sweetalert.min.js') !!}

        @include('partials.flash')

        @yield('scripts')
    </body>
</html>
