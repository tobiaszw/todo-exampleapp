@extends('layout')

@section('body')
<div class="col-md-8">
    {!! Form::model($project, ['route' => ['projects.update', $project->slug], 'method' => 'PUT']) !!}
    @include ('projects.partials.form')

    <div class="btn-group" role="group">
        <a href="{{ route('projects.index') }}" class="btn btn-primary btn-info">{{ trans('back')}}</a>
        {!! Form::submit('save', ['class'=>'btn btn-primary btn-success']) !!}
    </div>
    {!! Form::close() !!}
</div>
@stop