<fieldset class="form-group">
    {!! Form::label('name') !!}
    {!! Form::text('name', null, ["class" => 'form-control']) !!}
</fieldset>
<fieldset class="form-group">
    {!! Form::label('slug') !!}
    {!! Form::text('slug', null, ["class" => 'form-control', 'disabled' => 'disabled']) !!}
</fieldset>

