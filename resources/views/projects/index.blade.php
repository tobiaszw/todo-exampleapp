@extends('layout')

@section('body')
@if($projects)
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Slug</th>
            <th colspan='2'>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($projects as $project)
        <tr>
            <td>
                {{ $project->id }}
            </td>
            <td>
                <a href="{{ url('projects', $project->slug) }}">
                    {{ $project->name }}
                </a>
            </td>
            <td>
                {{ $project->slug }}
            </td>
            <td>
                <a href="{{ route('projects.edit', $project->slug) }}" class="btn btn-primary btn-info">{{ trans('edit')}}</a>
            </td>
            <td>
                {!! Form::open(['url' => route('projects.destroy', $project->slug), 'method'=>'delete']) !!}
                <button type="submit" class="btn btn-danger">
                    &times;
                </button>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
@stop