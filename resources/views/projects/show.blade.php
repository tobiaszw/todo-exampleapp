@extends('layout')

@section('style')
<style>
    .task-completed {
        text-decoration: line-through;
    }
</style>
@stop

@section('body')
<div class="col-md-8">
    <div class="pull-right add-bottom">
        <a href="{{ route('projects.edit', $project->slug)}}" class="btn btn-info">
            edit
        </a>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>
                Project
            </th>
            <td>
                {{ $project->name }} 
            </td>
        </tr>
        <tr>
            <th>Tasks</th>
            <td>
                {!! Form::open([ 'url' => route('tasks.store')]) !!}
                <div class="form-group">
                    <label for="newTask">New Task</label>
                    <input id="newTask" name="name" value="" required="" class="form-control">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                    <button type="submit" data-new-task class="btn btn-primary btn-success">add</button>
                </div>
                {!! Form::close() !!}
                <hr>
                @if ($project->tasks)  
                <ol data-tasks>
                    @foreach ($project->tasks as $task)
                    <li>
                        <label @if($task->completed)class="task-completed" @endif>
                                <input type="checkbox" data-complete-task="{{$task->id}}" @if($task->completed) checked=checked @endif>
                               {{$task->name}}
                        </label>
                    </li>
                    @endforeach   
                </ol>
                @else
                You have no task in current project, create one
                @endif
            </td>
        </tr>
    </table> 
</div>
@stop

@section('scripts')
<script>
    $(function () {

        var $list = $('[data-tasks]');
        $('[data-new-task]').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var $form = $(this).closest('form');
            var $that = $(this);
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (response) {

                    var task = response.task;
                    $list.append('<li data-id="' + task.id + '"><input type="checkbox"  data-complete-task="' + task.id + '" ><label>' + task.name + '</label></li>');
                    $that.val("");
                    $that.removeClass('btn-danger').addClass('btn-success');
                },
                error: function () {
                    $that.removeClass('btn-success').addClass('btn-danger');
                }
            });
        });

        $('[data-complete-task]').on('click', function () {
            var id = $(this).data('complete-task');
            var $that = $(this);
            $.ajax({
                type: "PUT",
                data: {
                    '_token': '{{{ csrf_token() }}}'
                },
                url: '/tasks/' + id,
                success: function (response) {
                    if (response.taskState == true)
                        $that.closest('label').addClass('task-completed');
                    else
                        $that.closest('label').removeClass('task-completed');
                }
            });
        });
    });
</script>
@stop