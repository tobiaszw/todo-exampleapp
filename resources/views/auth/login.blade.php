@extends('layout')

@section('style')
<style>
    .form-signin {
        max-width: 400px;
        margin: 40px auto;
    }
</style>
@stop


@section('body')
<div class="form-signin">
    <form method="POST" action="/" >
        <div class="well">
            <h1>Login</h1>
            email: <strong>root@example.com</strong>
            pwd: <strong>qwe</strong>
        </div>
        {!! csrf_field() !!}

        <div>
            <label for="email">Email</label>
            <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control">
        </div>

        <div>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control">
        </div>

        <div>
            <input type="checkbox" name="remember"> Remember Me
        </div>

        <div>
            <button type="submit" class="btn btn-primary">Login</button>
        </div>
    </form>
</div>
@stop